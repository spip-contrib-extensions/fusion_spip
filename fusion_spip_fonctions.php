<?php

function fusion_get_id_final($id_objet, $objet, $site) {
	return sql_getfetsel(
		'id_final',
		'spip_fusion_spip',
		'site_origine = ' . sql_quote($site) . ' and id_origine = ' . (int)$id_objet . ' and objet = ' . sql_quote($objet)
	);
}

function fusion_get_id_origine($id_objet, $objet, $site) {
	return sql_getfetsel(
		'id_origine',
		'spip_fusion_spip',
		'site_origine = ' . sql_quote($site) . ' and  id_final = ' . (int)$id_objet . ' and objet = ' . sql_quote($objet)
	);
}