<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_fusion_spip_correspondances_charger_dist() {

	$bases  = array_column(sql_allfetsel('site_origine', 'spip_fusion_spip', '', 'site_origine'), 'site_origine');
	$objets = array_column(sql_allfetsel('objet', 'spip_fusion_spip', '', 'objet'), 'objet');

	$valeurs = array(
		'bases'      => $bases,
		'objets'     => $objets,
		'base'       => '',
		'objet'      => '',
		'id_origine' => '',
		'id_final'   => '',
	);

	return $valeurs;
}

function formulaires_fusion_spip_correspondances_verifier_dist() {
	$erreurs = array();
	
	if (_request('id_origine')) {
		$id_final = fusion_get_id_final(_request('id_origine'), _request('objet'), _request('base'));
		if($id_final) {
			set_request('id_final', $id_final);
		} else {
			set_request('id_final', '');
			$erreurs['id_origine'] = _T('fusion_spip:aucune_correspondance');		
		}
	} else if (_request('id_final')) {
		$id_origine = fusion_get_id_origine(_request('id_final'), _request('objet'), _request('base'));
		if($id_origine) {
			set_request('id_origine', $id_origine);
		} else {
			set_request('id_origine', '');
			$erreurs['id_final'] = _T('fusion_spip:aucune_correspondance');
		}
	}
	
	return $erreurs;
}