<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/fusion_spip.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'fusion_spip_description' => 'Importer tous les contenus d’un site source, objets, liaisons, documents en mettant à jour les identifiants des objets, les liens internes et les modèles.',
	'fusion_spip_slogan' => 'Importer et fusionner tout le contenu d’un autre site'
);
