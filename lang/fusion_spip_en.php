<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/fusion_spip?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucune_correspondance' => 'No matches were found',

	// B
	'base' => 'Source site',
	'base_desc' => 'The source site database must be<a href=""/ecrire/?exec=admin_tech">declared as an external database</a>.<br/>
	The base of the source site must be in the same version as the host site',
	'bouton_importer' => 'Start to merge',
	'bouton_supprimer' => 'Delete the merge',

	// C
	'confirme_traduire_documents_doublons' => 'Translate duplicate documents using <multi> tags.',
	'confirme_warning' => 'Confirm the merge of the databases?',

	// D
	'dossier_existe_pas' => 'The directory @dossier@ doesn’t exist',
	'dossier_pas_lisible' => 'The directory @dossier@ cannot be read',

	// E
	'erreur_img_accessible' => 'Your IMG directory is not accessible. It is impossible either to write a file to the root, or to create a subdirectory.',
	'erreur_source_inaccessible' => 'Error reading the source IMG directory ',
	'erreur_traduction_document' => 'Your databases are in different languages, ticking the following box, you can translate the contents of duplicate documents using <multi> tags.',
	'erreur_version_indeterminee' => 'undefined (key version_installee of table spip_meta not found) ',
	'erreur_versions' => 'The host site and the source site are not in the same database version:
		<br/>- host is in version: @vhote@
		<br/>- source is in version: @vsource@',
	'erreur_versions_impossible' => 'Impossible to check the version of the imported database (table spip_meta not found) ',
	'explication_correspondance' => 'Enter the initial identifier to obtain the final identifier or vice versa.',

	// I
	'id_final' => 'N° after the merger',
	'id_origine' => 'N° before the merger',
	'img_dir' => 'Physical path of the documents',
	'img_dir_desc' => 'To copy the documents from the source site in the host site, indicate their physical path (absolute path on the hard disk, for example <code>/home/edgard/www/edgard_spip/IMG</code>). If the field is empty, no document will be imported, you will have to copy them manually.',

	// M
	'maj_base' => 'Update of the database',
	'manque_champs_hote' => 'The fields @diff@ are missing in the "@table@" table of the host base',
	'manque_champs_source' => 'The fields "@diff@" are missing in the table "@table@" of the source database',
	'manque_table_source' => 'The table "@table@" is missing in the source database',
	'message_img_dir_nok' => 'Please precise the path',
	'message_import_nok' => 'Error during the merge',
	'message_import_ok' => 'Fusion ended<br>detailed log: <code>tmp/log/fusion_spip_fusion_spip*.log</code><br><br>Here is a summary of the imported objects:<br>',
	'message_suppression_ok' => 'Deleted objects',

	// O
	'objets' => 'Object type',

	// R
	'referers' => 'Do not process referrers (inbound links)',

	// S
	'secteur' => 'Sector',
	'secteur_desc' => 'To import the source site  in a sector, otherwise it will be imported at the root',
	'stats' => 'Do not process statistics',

	// T
	'titre_fusion_spip' => 'Spip Websites Fusion',
	'titre_fusion_spip_correspondances' => 'Matches after merger',
	'titre_fusion_spip_suppression' => 'Deletion'
);
