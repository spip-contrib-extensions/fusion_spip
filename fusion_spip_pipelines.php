<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Avant la fusion, chercher les champs TIMESTAMP et désactiver ON UPDATE CURRENT_TIMESTAMP
 * pour bien garder les dates de mises à jour des objets éditoriaux importés
 * 
 * @param $flux
 *
 * @return mixed
 */
function fusion_spip_pre_fusion($flux) {
	$tables_autoupdate = array();
	ecrire_config('fusion_spip/tables_autoupdate', $tables_autoupdate);
	
	$lister_tables_principales = charger_fonction('lister_tables_principales', 'fusion_spip');
	$principales = $lister_tables_principales();
	foreach ($principales as $nom_table => $desc) {
		// on a besoin du shema complet, avec notamment la description des champs TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		// qui n'est pas présente dans la description retournée par lister_tables_principales()
		$shema = sql_showtable($nom_table, true);

		$autoupdate_field = null;
		foreach ($shema['field'] as $field => $value) {
			if(strpos($value, 'ON UPDATE CURRENT_TIMESTAMP')!==false){
				$tables_autoupdate[$nom_table] = $field;
			}
		}
	}
	// stocker les champs trouvés pour pouvoir les réactiver ensuite
	ecrire_config('fusion_spip/tables_autoupdate', $tables_autoupdate);
	
	foreach ($tables_autoupdate as $nom_table => $field) {
		sql_alter("TABLE $nom_table CHANGE $field $field TIMESTAMP NULL");
		fusion_spip_log("Champ $field : auto update désactivé", 'fusion_spip_autoupdate');
	}
	
	return $flux;
}

/**
 * Après la fusion, réactiver ON UPDATE CURRENT_TIMESTAMP
 *  
 * @param $flux
 *
 * @return mixed
 */
function fusion_spip_post_fusion($flux) {
	$tables_autoupdate = lire_config('fusion_spip/tables_autoupdate');
	foreach ($tables_autoupdate as $nom_table => $field) {
		sql_alter("TABLE $nom_table CHANGE $field $field TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
		fusion_spip_log("Champ $field : auto update réactivé", 'fusion_spip_autoupdate');
	}

	return $flux;
}

