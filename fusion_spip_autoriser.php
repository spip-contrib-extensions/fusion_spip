<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser
 */
function fusion_spip_autoriser() {
}

/**
 * Par défaut seuls les webmestres peuvent fusionner
 *
 * @param $faire
 * @param $type
 * @param $id
 * @param $qui
 * @param $opt
 *
 * @return bool
 */
function autoriser_fusionspip_menu($faire, $type, $id, $qui, $opt) {
	if ($qui['webmestre'] == 'oui') {
		return true;
	}
	return false;
}

/**
 * Par défaut seuls les webmestres peuvent accéder, et uniquement s'il y a des données
 *
 * @param $faire
 * @param $type
 * @param $id
 * @param $qui
 * @param $opt
 *
 * @return bool
 */
function autoriser_fusionspipcorrespondances_menu($faire, $type, $id, $qui, $opt) {
	if ($qui['webmestre'] == 'oui' && sql_countsel('spip_fusion_spip')) {
		return true;
	}
	return false;
}
