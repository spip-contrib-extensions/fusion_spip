# Fusion de SPIP

![fusion_spip](./prive/themes/spip/images/fusion_spip-xx.svg)

Importer et fusionner tout le contenu d’un autre site : textes, images, auteurs, liens, et sans se mélanger les pinceaux.

## Documentation

https://contrib.spip.net/Fusion-de-SPIP
